# Python React Simple App

Python API Tech Stack

- Flask 
- Flask RESTful
- SQLAlchemy
- JWT
- Docker Setup 
- Flask Environment

Run Command:
flask run

React Frontend Tech Stack

- Redux
- thunk
- babel

Run Command:
npm start

Disciplined project structure explained here.   